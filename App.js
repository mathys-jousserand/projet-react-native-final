import React, { useState } from 'react';
import { Button, View, Stylesheet } from 'react-native';
import Constants from 'expo-constants';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Home from './pages/Home';
import Local from './pages/meteo_locale';

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button onPress={() => navigation.goBack()} title="Go back home" />
    </View>
  );
}

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator
        drawerStyle={{
          backgroundColor: '#E6F6F6',
          width: 240,
        }}
        initialRouteName="Home"
        header={{
          tintColor: 'rgba(51, 75, 51, 1)',
        }}
        drawerContentOptions={{
          activeTintColor: '#FFB578',
        }}>
        <Drawer.Screen name="Accueil" component={Home} />
        <Drawer.Screen name="Météo locale" component={Local} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
