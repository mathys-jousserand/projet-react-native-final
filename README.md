# Sample Snack app

Bonjour!

Cette application a pour but de donner la météo en temps réel en fonction de la 
géolocalisation de l’utilisateur.

Pour cela la position de l’utilisateur est récupérée et cela nous donne la longitude et la latitude de l’utilisateur et ainsi il est possible de les renseigner dans l’url de l’api afin d’afficher les données souhaitées en fonction de la position de l’utilisateur. L’api utilisée est Open weather map ( https://openweathermap.org/ ) et l’application récupère la température en degrés fahrenheit °F qui est ensuite transformée en °C via une formule mathématique, ainsi que le pays, la ville et les conditions météorologiques. Les conditions météo font changer le contenu en fonction de leur résultat, par exemple s’il pleut le background sera bleu, une icône de nuage pluvieux sera affiché ainsi qu’un texte correspondant au temps. 
L’application contient un menu burger afin d’accéder aux différentes pages à partir de n’importe quelle page.

Voici le lien vers le dépôt GIT: (https://gitlab.com/mathys-jousserand/projet-react-native-final)





