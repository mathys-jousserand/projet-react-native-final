import React from 'react';
import { StyleSheet, Text, View, Animated } from 'react-native';


import { API_KEY } from '../utils/WeatherAPIKey';

import Weather from '../components/Weather';

export default class App extends React.Component {
  state = {
    isLoading: true,
    temperature: 0,
    weatherCondition: null,
    city: 'Yssingeaux',
    country: 'FR',
    error: null,
  };

  componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.fetchWeather(position.coords.latitude, position.coords.longitude);
      },
    );
  }

  fetchWeather(lat, lon) {
    fetch(
      `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=imperial`
    )
      .then((res) => res.json())
      .then((json) => {
        // console.log(json);
        this.setState({
          temperature: Math.round((json.main.temp-32)/1.8),
          weatherCondition: json.weather[0].main,
          city: json.name,
          country: json.sys.country,
          isLoading: false,
        });
      });
  }

  render() {
    const {
      isLoading,
      weatherCondition,
      temperature,
      city,
      country,
    } = this.state;
    return (
      <View style={styles.container}>
        {isLoading ? (
          <View style={styles.loadingContainer}>
            <Text style={styles.loadingText}>Affichage de la Météo</Text>
            <Text style={styles.loadingDots}>...</Text>
          </View>
        ) : (
          <Weather
            weather={weatherCondition}
            temperature={temperature}
            city={city}
            country={country}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loadingContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E6F6F6',
  },
  loadingText: {
    fontSize: 30,
    color:'#FFB578',
  },
  loadingDots: {
    fontSize: 80,
    color:'#FFB578',
  }
});
