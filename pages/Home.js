import { StyleSheet, Button, View, Text, Image } from 'react-native';
import React, { useState } from "react";
import Constants from 'expo-constants';
export default function Home({ navigation }) {
    
  const [count, setCount] = useState(0);
  const [theme, setTheme] = useState(styles.containerDark);

	function buttonClickHandler() {
		setCount(count + 1);
    console.log(count);
	}

	function themeHandlerDark() { setTheme(styles.containerDark,styles.paragraphDark);}
  function themeHandlerLight() { setTheme(styles.containerLight,styles.paragraphLight);}

  return (
    <View style={theme}>
      <Image style={styles.logo} source={require('../assets/weather-up_logo-star.png')}/>
      <Text style={styles.paragraph}>
        Bienvenue sur Weather Up, votre application météorologique en temps réel.
      </Text>
        <Button
				  onPress={themeHandlerDark}
				  title="Dark"
				  color='#FFB578'
			  />
        <Button
				  onPress={themeHandlerLight}
				  title="Light"
				  color="#B3EEEE"
			  />
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    width:'80%',
    height:'42%',
  },
    paragraph: {
    width:'80%',
    paddingTop:'30%',
    paddingBottom:'30%',
    textAlign:'center',    
    color:'#FFB578',
  },
  containerLight: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' , 
    backgroundColor:'#FFFFFF',
  },
  containerDark: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center' , 
    backgroundColor:'#373737',
  },
});
