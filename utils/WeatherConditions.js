export const weatherConditions = {
  Rain: {
    color: '#B3EEEE',
    title: 'Pluvieux',
    subtitle: 'Il pleut, n oubliez pas votre parapluie.',
    icon: 'weather-rainy'
  },
  Clear: {
    color: '#FFB578',
    title: 'Ensoleillé',
    subtitle: 'Le soleil brille, profitez en !',
    icon: 'weather-sunny'
  },
  Thunderstorm: {
    color: '#55B4B4',
    title: 'Orageux',
    subtitle: 'Faites attention le temps est agité',
    icon: 'weather-lightning'
  },
  Clouds: {
    color: '#FFC89A',
    title: 'Nuageux',
    subtitle: 'Le ciel est plutôt couvert actuellement.',
    icon: 'weather-cloudy'
  },

  Snow: {
    color: '#00d2ff',
    title: 'Eneigé',
    subtitle: 'Installez vous confortablement près de la cheminée.',
    icon: 'weather-snowy'
  },
  Drizzle: {
    color: '#82D7D7',
    title: 'Un peu pluvieux',
    subtitle: 'Il pleuvine, couvrez-vous.',
    icon: 'weather-hail'
  },
  Haze: {
    color: '#77A6A6',
    title: 'Brumeux',
    subtitle: 'La visibilité est un peu réduite.',
    icon: 'weather-hail'
  },
  Mist: {
    color: '#5B7272',
    title: 'Brouillard',
    subtitle: "Faites attention la visibilité est reduite.",
    icon: 'weather-fog'
  }
};
